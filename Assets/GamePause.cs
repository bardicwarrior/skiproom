﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GamePause : MonoBehaviour
{
    public string pauseScene;
    public BoolVariable isGamePaused;
    AudioSource gameBGM;

    private void Start() {
        GameObject bgmOb = GameObject.FindGameObjectWithTag("GameBGM");
        gameBGM = bgmOb.GetComponent<AudioSource>();
    }

    public void PauseGame(InputAction.CallbackContext ctx) {
        if (ctx.performed) {
            if (!isGamePaused.Value) {
                Pause();
            } else {
                UnPause();
            }
        }
    }

    public void Pause() {
        Time.timeScale = 0;
        isGamePaused.SetValue(true);
        gameBGM.Pause();

        SceneManager.LoadScene(pauseScene, LoadSceneMode.Additive);
    }

    public void UnPause() {
        Scene scene = SceneManager.GetSceneByName(pauseScene);
        if (scene.IsValid()) {
            SceneManager.UnloadSceneAsync(scene);
        }
        SceneManager.UnloadSceneAsync(pauseScene);

        isGamePaused.SetValue(false);
        gameBGM.UnPause();
        Time.timeScale = 1;
    }
}
