﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinCutScene : MonoBehaviour
{

    public AudioSource aS;
    public float camSpeed = 1.0f;

    CinemachineVirtualCamera cm;
    GameObject player;

    bool moveCamera;
    // Start is called before the first frame update
    void Start()
    {
    }

    private void OnEnable() {
        player = GameObject.FindGameObjectWithTag("Player");
        PlayEndScene();
    }

    // Update is called once per frame
    void Update()
    {
        if (moveCamera) {
            transform.Translate(Vector3.down * camSpeed * Time.deltaTime);
        }
    }

    public void PlayEndScene() {
        
        // Stop all Player Input
        player.GetComponent<Player>().StopInput(); 

        // PREPARE ALL THE VARS
        cm = GameObject.FindGameObjectWithTag("CineCamera").GetComponent<CinemachineVirtualCamera>();
        cm.Follow = transform;
        AudioSource gameBGM = GameObject.FindGameObjectWithTag("GameBGM").GetComponent<AudioSource>();

        // Fade the music out and fade winning music in
        StartCoroutine(FadeAudioSource.StartFade(gameBGM, 4.0f, 0.0f));
        aS.Play();
        StartCoroutine(FadeAudioSource.StartFade(aS, 6.0f, 0.5f));

        // Pan the camera down (prisoners jumping and doors opening are on simple triggers)
        Invoke("MoveCamera", 1.5f);

        // Fade In Game Complete Scene
        Invoke("CompleteScene", 10.0f);
        
    }

    public void MoveCamera() {
        moveCamera = true;
    }

    public void CompleteScene() {
        SceneManager.LoadSceneAsync("GameComplete", LoadSceneMode.Additive);
    }
}
