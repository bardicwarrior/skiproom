﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour
{
    public TMPro.TextMeshProUGUI[] text;
    public Image[] images;
    public float fadeTime = 3.0f;
    // Start is called before the first frame update
    void Start()
    {
        foreach (TMPro.TextMeshProUGUI item in text) {
            item.CrossFadeAlpha(0.0f, 0.0f, false);

            item.CrossFadeAlpha(1.0f, fadeTime, false);
        }

        foreach (Image item in images) {
            item.CrossFadeAlpha(0.0f, 0.0f, false);

            item.CrossFadeAlpha(1.0f, fadeTime, false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
