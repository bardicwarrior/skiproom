﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour
{
    public AudioClip audioClip;
    public bool loop;
    public bool playAtStart;
    public float fadeInTime;

    AudioController nonPooledAudioController;
    bool noPool;

    private void Start() {
        // Check if there is an AudioController on this obj - if there is do NOT use the ObjectPooler
        AudioController aC = GetComponent<AudioController>();
        if (aC != null) {
            nonPooledAudioController = aC;
            noPool = true;
        }

        if (playAtStart) PlaySound();
    }

    public void PlaySound() {
        AudioController audioController;

        if (noPool) {
            audioController = nonPooledAudioController;
        } else {
            GameObject pooledAudio = ObjectPooler.SharedInstance.GetPooledObject();
            audioController = pooledAudio.GetComponent<AudioController>();
        }

        if (fadeInTime > 0.0f) {
            audioController.PlayClipFadeIn(audioClip, loop, fadeInTime);
        } else {
            audioController.PlayClip(audioClip, loop);
        }
    }
}
