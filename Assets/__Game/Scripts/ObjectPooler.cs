﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler SharedInstance;
    public List<GameObject> pooledObjects = new List<GameObject>();
    public GameObject objectToPool;
    public int amountToPool;
    public GameObject[] enableWhenPrepped; // For objects that grab a pooled object on start/enable, this avoids a race condition where the pool isnt ready yet

    private void Awake() {
        SharedInstance = this;
    }

    private void Start() {
        for (int i = 0; i < amountToPool; i++) {
            GameObject obj = Instantiate(objectToPool);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
        foreach (GameObject item in enableWhenPrepped) {
            item.SetActive(true);
        }
    }

    public GameObject GetPooledObject() {
        for (int i = 0; i < pooledObjects.Count; i++) {
            if (!pooledObjects[i].activeInHierarchy) {
                return pooledObjects[i];
            }
        }

        return null;
    }
}
