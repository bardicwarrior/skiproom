﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SimpleTrigger : MonoBehaviour
{
    public UnityEvent onTriggered;
    public bool singleUse;
    private bool triggered;
    public float delay;
    public string triggerOnTag;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (!triggered && (string.IsNullOrEmpty(triggerOnTag) || collision.CompareTag(triggerOnTag))) {
            if (singleUse) triggered = true;
            StartCoroutine(FireOnTriggered());
        }
    }

    //private void OnCollisionEnter2D(Collision2D collision) {
    //    if (!triggered) {
    //        if (singleUse) triggered = true;
    //        StartCoroutine(FireOnTriggered());
    //    }
    //}
    
    IEnumerator FireOnTriggered() {
        yield return new WaitForSeconds(delay);
        onTriggered.Invoke();
        if (singleUse) enabled = false;
    }

    public void FireIn(UnityEvent fire, float secs) {
        StartCoroutine(FireInX(fire, secs));
    }

    IEnumerator FireInX(UnityEvent fire, float secs) {
        yield return new WaitForSeconds(secs);
        fire.Invoke();
    }
        
}
