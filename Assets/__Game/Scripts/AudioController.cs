﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioSource audioSource;

    public void PlayClip(AudioClip clip, bool loop) {
        if (loop) PlayClipLoop(clip);
        else PlayClipOnce(clip);
    }

    public void PlayClipOnce(AudioClip clip) {
        gameObject.SetActive(true);
        audioSource.PlayOneShot(clip);
        Invoke("SetToInactive", clip.length);
    }
    public void PlayClipLoop(AudioClip clip) {
        gameObject.SetActive(true);
        audioSource.clip = clip;
        audioSource.Play();
    }

    public void PlayClipFadeIn(AudioClip clip, bool loop, float fadeInTime) {
        gameObject.SetActive(true);
        audioSource.clip = clip;
        float initVolume = audioSource.volume;
        audioSource.volume = 0.0f;
        audioSource.Play();
        StartCoroutine(FadeAudioSource.StartFade(audioSource, fadeInTime, initVolume));
        if (!loop) Invoke("SetToInactive", clip.length);
        else audioSource.loop = true;
    }

    void SetToInactive() {
        gameObject.SetActive(false);
    }
}
