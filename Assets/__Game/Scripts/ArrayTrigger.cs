﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

public class ArrayTrigger : MonoBehaviour
{
    public GameObject[] objects;
    public string componentName;
    Type componentType;

    public string methodName;
    MethodInfo methodInfo;
    public bool singleUse;
    
    private bool triggered;
    public float delay;
    public string triggerOnTag;
    
    // Start is called before the first frame update
    void Start() {
        componentType = Type.GetType(componentName);
        methodInfo = componentType.GetMethod(methodName);
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (!triggered && (string.IsNullOrEmpty(triggerOnTag) || collision.CompareTag(triggerOnTag))) {
            if (singleUse) triggered = true;
            StartCoroutine(FireOnTriggered());
        }
    }

    IEnumerator FireOnTriggered() {
        yield return new WaitForSeconds(delay);
        foreach (GameObject item in objects) {
            Component comp = item.GetComponent(componentName);
            methodInfo.Invoke(comp, null);
        }
        if (singleUse) enabled = false;
    }

    public void FireIn(UnityEvent fire, float secs) {
        StartCoroutine(FireInX(fire, secs));
    }

    IEnumerator FireInX(UnityEvent fire, float secs) {
        yield return new WaitForSeconds(secs);
        fire.Invoke();
    }
}
