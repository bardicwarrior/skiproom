﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public int current = 1;
    public int max = 1;

    public float damageCooldown = 1.0f;
    float damageTimer = 0.0f;
    
    [Tooltip("Will fire when health value becomes zero")]
    public UnityEvent zeroHealthEvent;
    [Tooltip("Will fire anytime health value changes, including when zero")]
    public UnityEvent healthChangeEvent;
    [Tooltip("Will fire when health value changes to anything that is not zero")]
    public UnityEvent healthChangeNotZeroEvent;


    private void Start() {
        current = max;
    }

    private void Update() {
        if (damageTimer < damageCooldown) {
            damageTimer += Time.deltaTime;
        }
    }

    public void TakeDamage(int damage, bool ignoreCooldown) {
        if (ignoreCooldown || damageTimer >= damageCooldown) {
            if (current > 0) {
                current -= damage;
                if (current <= 0) {
                    current = 0;
                    zeroHealthEvent.Invoke();
                } else {
                    if (!ignoreCooldown) damageTimer = 0.0f;
                    healthChangeNotZeroEvent.Invoke();
                }
                healthChangeEvent.Invoke();
            }
        }

    }

    public void TakeDamage(int damage) {
        TakeDamage(damage, false);
    }

    public void HealDamage(int heal)
    {
        TakeDamage(heal * -1);
        if (current > max) current = max;
    }

    public bool IsZeroHealth()
    {
        return current <= 0;
    }

    public void HealToFull() {
        current = max;
        healthChangeEvent.Invoke();
    }

    public bool DamageWouldKill(int damage) {
        return current - damage <= 0;
    }
}
