﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public bool chaseTarget;
    bool facingRight = true;

    public float minSpeed = 3.0f;
    public float maxSpeed = 5.0f;
    float speed = 4.0f;
    public float attackRadius = 1.0f;
    public float attackCooldown = 1.0f;

    float attackTimer = 0.0f;

    Transform target;
    Rigidbody2D rb;
    Animator anim;
    SpriteRenderer sprite;
    Health hp;

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        hp = GetComponent<Health>();
        facingRight = transform.localScale.Equals(Vector3.one);
        speed = (Random.value * (maxSpeed - minSpeed)) + minSpeed;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void FixedUpdate() {
        if (attackTimer < attackCooldown) {
            attackTimer += Time.deltaTime;
        }

        if (chaseTarget) {
            bool playerInAttackRadius = Vector2.Distance(transform.position, target.position) <= attackRadius;
            if (playerInAttackRadius && attackTimer > attackCooldown) {
                anim.SetTrigger("attack");
                attackTimer = 0.0f;
            }

            Vector2 direction = target.position - transform.position;

            if (direction.x > 0 && !facingRight) {
                transform.localScale = Vector3.one;
                facingRight = true;
            } else if (direction.x < 0 && facingRight) {
                transform.localScale = new Vector3(-1, 1, 1);
                facingRight = false;
            }

            Vector2 relPlayerDir = new Vector2(Mathf.Sign(direction.x), Mathf.Sign(direction.y));
            anim.SetFloat("relPlayerY", relPlayerDir.y);

            if (!playerInAttackRadius) {
                rb.MovePosition(Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime));
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("PlayerAttack")) {
            hp.TakeDamage(1);
        }
    }

    public void ChasePlayer() {
        chaseTarget = true;
        anim.SetBool("chase", chaseTarget);
    }

    public void Damage() {
        anim.SetTrigger("damage");
    }

    public void Die() {
        anim.SetTrigger("die");
        rb.simulated = false;
        enabled = false;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + new Vector3(attackRadius, 0));
    }
}
