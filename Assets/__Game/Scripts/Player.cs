﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField]
    AnimationClip attackAnimation;
    [SerializeField]
    float attackAnimationSpeed = 1.0f;
    [Tooltip("If 0, uses attackAnimation's time")]
    public float attackCooldown = 1.0f;

    [SerializeField]
    AnimationClip damageClip;

    [SerializeField]
    AudioPlayer hurtPlayer; // could go into a state behavior for the damage state animation

    public float moveSpeed = 5.0f;
    public Vector2 move;
    public bool canMove = true;
    public GameObject inputHandler;

    float attackTimer = 0.0f;
    bool canAttack = true;
    Vector2 newVelocity;
    Rigidbody2D rb;
    Animator anim;
    SpriteRenderer sprite;
    Health hp;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();
        hp = GetComponent<Health>();
    }
    // Start is called before the first frame update
    void Start()
    {
        if (attackCooldown == 0.0f) {
            attackCooldown = attackAnimation.length / attackAnimationSpeed;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate() {
        if (attackTimer < attackCooldown) {
            attackTimer += Time.deltaTime;
        }

        ApplyMove();
    }
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("EnemyAttack")) {
            hp.TakeDamage(1);
        }
    }

    void ApplyMove() {
        if (canMove) {
            newVelocity.Set(moveSpeed * move.x, moveSpeed * move.y);
            rb.velocity = newVelocity;
        } else {
            rb.velocity = Vector2.zero;
        }
    }
    
    /**** INPUT FUNCTIONS ****/

    public void Move(InputAction.CallbackContext ctx) {
        move = ctx.ReadValue<Vector2>();
        anim.SetBool("moving", (move.x != 0 || move.y != 0) );

        // Do not want the sprite to flip/change if attack is in prog TODO##
        if (move.x > 0) transform.localScale = Vector3.one;
        else if (move.x < 0) transform.localScale = new Vector3(-1, 1, 1);

        if (move.y != 0) anim.SetFloat("moveY", move.y);
    }

    public void Attack(InputAction.CallbackContext ctx) {
        if (attackTimer > attackCooldown) {
            anim.SetTrigger("attack");
            attackTimer = 0.0f;
        }
    }

    public void Die() {
        GameObject bgmOb = GameObject.FindGameObjectWithTag("GameBGM");
        AudioSource gameBGM = bgmOb.GetComponent<AudioSource>();
        gameBGM.Stop();

        enabled = false;
        inputHandler.SetActive(false);
        anim.ResetTrigger("attack");
        anim.speed = .5f;
        Damage();

        Enemy[] enemies = FindObjectsOfType<Enemy>();
        foreach (Enemy item in enemies) {
            item.enabled = false;
            item.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            item.GetComponent<Animator>().speed = 0;
        }

        anim.SetTrigger("die");
        StartCoroutine(LoadGameOver());
    }

    public void Damage() {
        anim.SetTrigger("damage");
        hurtPlayer.PlaySound();
        canAttack = false;
        StartCoroutine(CanAttack(damageClip.length));
    }

    public IEnumerator LoadGameOver() {
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadSceneAsync("GameOver", LoadSceneMode.Additive);
    }

    public IEnumerator CanAttack(float time) {
        yield return new WaitForSeconds(time);
        canAttack = true;
    }

    public void StopInput() {
        inputHandler.SetActive(false);
    }
}
