﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoolReference
{
    public bool UseConstant = true;
    public bool ConstantValue;
    public BoolVariable Variable;

    public BoolReference() {

    }

    public BoolReference(bool value) {
        UseConstant = true;
        ConstantValue = value;
    }

    public bool Value {
        get { return UseConstant ? ConstantValue : Variable.Value; }
        set {
            if (UseConstant) ConstantValue = value;
            else Variable.SetValue(value);
        }
    }

    public static implicit operator bool(BoolReference reference) {
        return reference.Value;
    }
}
