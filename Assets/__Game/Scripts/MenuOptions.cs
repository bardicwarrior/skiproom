﻿using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuOptions : MonoBehaviour
{
    public BoolVariable isGamePaused;
    public string[] newGameScenes;
    public string pauseScene;
    public TextMeshProUGUI startResumeText;

    private void Start() {
        if (isGamePaused.Value) {
            if (startResumeText != null) {
                startResumeText.SetText("Resume");
            }
        }
    }

    public void NewGame() {
        if (!isGamePaused.Value) {
            for (int i = 0; i < newGameScenes.Length; i++) {
                if (i == 0) {
                    SceneManager.LoadScene(newGameScenes[i], LoadSceneMode.Single);
                } else {
                    SceneManager.LoadScene(newGameScenes[i], LoadSceneMode.Additive);
                }
            }
        } else {
            UnpauseGame();
        }
    }

    public void LoadGame() {

    }

    public void Options() {

    }

    public void QuitGame() {
        isGamePaused.SetValue(false);
#if UNITY_STANDALONE
        Application.Quit();
#endif

#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }

    public void PauseGame() {
        if (!isGamePaused) {
            Time.timeScale = 0;
            isGamePaused.SetValue(true);
            SceneManager.LoadScene(pauseScene, LoadSceneMode.Additive);
        } 
    }

    public void UnpauseGame() {
        if (isGamePaused) {

            GameObject player = GameObject.FindGameObjectWithTag("Player");
            if (player != null) {
                player.GetComponent<GamePause>().UnPause();
            } else {
                Time.timeScale = 1;
                isGamePaused.SetValue(false);
                Scene scene = SceneManager.GetSceneByName(pauseScene);
                if (scene.IsValid()) {
                    SceneManager.UnloadSceneAsync(scene);
                }
            }
        }
    }
}
