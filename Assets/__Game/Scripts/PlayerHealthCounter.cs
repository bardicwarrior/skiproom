﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthCounter : MonoBehaviour
{
    public GameObject healthPrefab;
    public Sprite noHealthSprite;
    GameObject[] healthContainers;
    public Health playerHealth;
    public bool updateHealth;

    // Start is called before the first frame update
    void Start()
    {
        if (healthPrefab == null) {
            Debug.LogError("!!!!! Need health prefab on " + this.name);
            enabled = false;
            return;
        }

        if (playerHealth == null) {
            Debug.LogWarning("#### PlayerHealth not set in UI Counter - recommend setting manually... But Ill do it just for you");
            GameObject p = GameObject.FindGameObjectWithTag("Player");
            playerHealth = p.GetComponent<Health>();
        }
        playerHealth.healthChangeEvent.AddListener(UpdateHealthDisplay);
        playerHealth.zeroHealthEvent.AddListener(UpdateHealthDisplay);

        healthContainers = new GameObject[playerHealth.max];
        for (int i = 0; i < playerHealth.max; i++) {
            healthContainers[i] =  Instantiate(healthPrefab, transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (updateHealth) UpdateHealthDisplay();
    }

    public void UpdateHealthDisplay() {
        updateHealth = false;
        for (int i = 0; i < playerHealth.max; i++) {
            //player has 1 health - index 0 would be 1
            // i = 0, health = 2 
            if (i < playerHealth.current) {
                healthContainers[i].SetActive(true);
            } else {
                healthContainers[i].GetComponent<Image>().sprite = noHealthSprite;
            }
        }
    }
}
